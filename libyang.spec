Name: libyang
Version: 1.0.184
Release: 6
Summary: YANG data modeling language library
Url: https://github.com/CESNET/libyang
Source: %{url}/archive/%{name}-%{version}.tar.gz
License: BSD

Patch0:	libyang-1.0.184-doc.patch
Patch1: CVE-2021-28903.patch
Patch2: CVE-2021-28905.patch
Patch3: CVE-2021-28904.patch
Patch4: CVE-2021-28902-CVE-2021-28906.patch

Requires:  pcre
BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  pcre-devel
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  swig >= 3.0.12
BuildRequires:  libcmocka-devel
BuildRequires:  python3-devel
BuildRequires:  flex
BuildRequires:  bison
BuildRequires:  graphviz

%package devel
Summary:    Development files for libyang
Requires:   %{name}%{?_isa} = %{version}-%{release}
Requires:   pcre-devel

%package devel-doc
Summary:    Documentation of libyang API
Requires:   %{name} = %{version}-%{release}
BuildArch:  noarch

%package -n libyang-cpp
Summary:    C++ bindings for libyang
Requires:   %{name}%{?_isa} = %{version}-%{release}

%package -n libyang-cpp-devel
Summary:    Development files for libyang-cpp
Requires:   libyang-cpp%{?_isa} = %{version}-%{release}
Requires:   pcre-devel

%package -n python3-yang
Summary:    Python3 bindings for libyang
Requires:   libyang-cpp%{?_isa} = %{version}-%{release}
%{?python_provide:%python_provide python3-libyang}

%description -n libyang-cpp
Bindings of libyang library to C++ language.

%description -n libyang-cpp-devel
Headers of bindings to c++ language.

%description -n python3-yang
Bindings of libyang library to python language.

%description devel
Headers of libyang library.

%description devel-doc
Documentation of libyang API.

%description
Libyang is YANG data modeling language parser and toolkit
written (and providing API) in C.

%prep
%autosetup -n %{name}-%{version} -p1

mkdir build && cd build
cmake \
   -DCMAKE_INSTALL_PREFIX:PATH=/usr \
   -DCMAKE_BUILD_TYPE:String="Package" \
   -DENABLE_LYD_PRIV=ON \
   -DGEN_JAVA_BINDINGS=OFF \
   -DGEN_JAVASCRIPT_BINDINGS=OFF \
   -DGEN_LANGUAGE_BINDINGS=ON \
   -DENABLE_VALGRIND_TESTS=OFF ..

make
make doc

%check
ctest --output-on-failure -V %{?_smp_mflags}

%install
cd build
%{__make} install DESTDIR=%{?buildroot} INSTALL="%{__install} -p"
cd ../
mkdir -m0755 -p %{buildroot}/%{_docdir}/libyang
cp -r doc/html %{buildroot}/%{_docdir}/libyang/html

%files
%license LICENSE
%{_bindir}/yanglint
%{_bindir}/yangre
%{_datadir}/man/man1/yanglint.1.gz
%{_datadir}/man/man1/yangre.1.gz
%{_libdir}/libyang.so.1
%{_libdir}/libyang.so.1.*
%{_libdir}/libyang1

%files devel
%{_libdir}/libyang.so
%{_libdir}/pkgconfig/libyang.pc
%{_includedir}/libyang/*.h
%dir %{_includedir}/libyang/

%files devel-doc
%{_docdir}/libyang

%files -n libyang-cpp
%{_libdir}/libyang-cpp.so.1
%{_libdir}/libyang-cpp.so.1.*

%files -n libyang-cpp-devel
%{_libdir}/libyang-cpp.so
%{_includedir}/libyang/*.hpp
%{_libdir}/pkgconfig/libyang-cpp.pc
%dir %{_includedir}/libyang/

%files -n python3-yang
%{python3_sitearch}/yang.py
%{python3_sitearch}/_yang.so
%{python3_sitearch}/__pycache__/yang*

%changelog
* Tue Mar 07 2023 zhouwenpei <zhouwenpei@h-partners.com> - 1.0.184-6
- remove debug_package %{nil} to strip files and provide debug packages

* Sat Dec 24 2022 qisen <qisen@huawei.com> - 1.0.184-5
- addyaml

* Wed Aug 25 2021 orange-snn <songnannan2@huawei.com> - 1.0.184-4
- change the python3 package name to python3-pyang from python3-libyang

* Tue Jun 29 2021 anaonymous_z <zhangrui182@huawei.com> - 1.0.184-3
- Type:CVE
- ID:NA
- SUG:NA
- DESC: fix CVE-2021-28902 CVE-2021-28906
        CVE-2021-28904

* Mon Jun 28 2021 zhuqingfu <zhuqingfu1@huawei.com> - 1.0.184-2
- Add patch CVE-2021-28905

* Wed Jun 23 2021 yaozc7 <yaozc7@foxmail.com> - 1.0.184-1
- Add patch CVE-2021-28903

* Fri Jul 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0.184-0
- Package init
